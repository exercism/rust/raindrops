//use std::collections::HashMap;

pub fn raindrops(number: u32) -> String {
    let mut sound: String = String::new();
    let pling: &str = "Pling";
    let plang: &str = "Plang";
    let plong: &str = "Plong";

    if number % 3 == 0 {
        sound.push_str(pling);
    }

    if number % 5 == 0 {
        sound.push_str(plang);
    }

    if number % 7 == 0 {
        sound.push_str(plong);
    }

    if sound.is_empty() {
        sound = number.to_string()
    }

    sound
}
